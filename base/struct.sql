create database Caisse;
use Caisse;

create table produit(
    id int primary key not null auto_increment,
    nom VARCHAR(250),
    prix double,
    stock int not null
)ENGINE=InnoDB;

create table caisse(
    id int primary key not null auto_increment,
    numero VARCHAR(250)
)ENGINE=InnoDB;

create table achat(
    id int primary key not null auto_increment,
    idCaisse int not null,
    idProduit int not null,
    quantite int not null,
    dateAchat date not null,
    foreign key (idCaisse) references caisse(id),
    foreign key (idProduit) references produit(id)
)ENGINE=InnoDB;

insert into produit(nom,prix,stock) values ("BonBon",200,10),("Biscuit",300,20),("Yaourt",500,19),("Coca",1200,25),("Chipss",500,13);
insert into caisse(numero) values ('caisse1'),('caisse2');
insert into achat (idCaisse,idProduit,quantite,dateAchat) VALUES
        ('2','1','5','curdate()');


CREATE view detailsAchat AS
    select c.id as idCaisse,p.nom,p.prix,a.quantite,(p.prix*a.quantite) as montant from achat a,caisse c,produit p
        where 
            a.idCaisse = c.id AND
                a.idProduit = p.id ;