<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('BaseController.php');

class Achat extends BaseController {

	public function __construct()
	{
		parent::__construct();
	}


    public function saisie(){ 
		$this->load->model('Produit');
		$this->load->model('model');
		$data['liCateg']=$this->model->get_Categ();
		$data['liProd']=$this->model->get_prod();
		$caisse=$this->session->userdata('idCaisse');
		$data['idCaisse']=$caisse;
		$data['vue']='saisie.php';
		$this->load->view('template',$data);

	}

	

	public function changerCaisse(){
		$this->load->model('Caisse');
		$c=new Caisse();
		$c=$c->listeCaisse();
		$data=array();
		$data["caisse"]=$c;
		$data['vue']='accueil.php';
		// $this->session->unset_userdata('idCaisse');
		$this->load->view('template',$data);
	}

	// public function getPanier(){
	// 	$this->load->model('model');
	// 	$idC=$this->session->userdata('idCaisse');
	// 	$data['liPanier']=$this->model->get_AchatbyIdCaisse($idC);
	// 	$data['idCaisse']=$idC
	// 	$data['vue']='saisie.php';
	// 	$this->load->view('template',$data);	


	// }
	

public function ListePanier(){
	$this->load->model('model');
	$caisse=$this->session->userdata('idCaisse');
	$args = array();
	$args['idCaisse'] = $caisse;
	$args['validite'] = 0;
	$produit=$this->model->get_produit_achat_categorie($args);
	$data=array();
	$data['idCaisse']=$caisse;
	$data['produit']=$produit;
	$data['vue']='panier.php';
	$this->load->view('template',$data);
}

public function valider(){
	$this->load->model('model');
	$this->model->validate_achat();
	$caisse=$this->session->userdata('idCaisse');
	$data['liProd']=$this->model->get_prod();
	$data['liCateg']=$this->model->get_Categ();
	$data['idCaisse']=$caisse;
	$data['vue']='saisie.php';
	$this->load->view('template',$data);
}

	
}
