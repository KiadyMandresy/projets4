<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BigController extends CI_Controller {

    
    public function getUrlLogin(){
        $data['vue']='login.php';
        $this->load->view('template',$data);
    }
    public function getUrlCreate(){
        $this->load->model('DBmodel');
        $data['listCate']=$this->DBmodel->get_categ();
        $data['vue']='create.php';
        $this->load->view('template',$data);
    }
    public function getUrlUpdate(){
        $this->load->model('DBmodel');
        $lol = $this->input->post('id');
        $data['getOne']=$this->DBmodel-> getOneProd($lol);
        $this->load->view('update',$data);
    }

    public function getUrlCrud(){
        $this->load->model('DBmodel');
        $cond=null;
        $data['listProd']=$this->DBmodel->getProduit($cond);
        $data['vue']='crud.php';
        $this->load->view('template',$data);
    }

    
    
    public function testLogin(){
        $this->load->model('DBmodel');
        $data['listAdmin']=$this->DBmodel->getListAdmin();
        $log=$this->input->post("login");
		$pwd=$this->input->post("mdp");
        echo $log;
        echo $pwd;
        for($i=0;$i<count($data['listAdmin']);$i++){
            if($data['listAdmin'][$i]['login']==$log && $data['listAdmin'][$i]['mdp']==sha1($pwd)){
                $cond=null;
                $data['listProd']=$this->DBmodel->getProduit($cond);
                $data['vue']='crud.php';
                $this->load->view('template',$data);
            }   
            else{
                $data['vue']='login.php';
                $this->load->view('template',$data);
            }
        }
       
	}

    public function insertProd(){
        $this->load->model('DBmodel');
        $tab=array();
        $tab[0] = $this->input->post("nom");
        $tab[1] = $this->input->post("categ");
        $tab[2] = $this->input->post("prix");
        $tab[3] = $this->input->post("nom");
        $this->DBmodel->insertProduit($tab);
        $data['listCate']=$this->DBmodel->get_categ();
        $data['vue']='create.php';
        $this->load->view('template',$data);
    }

    public function update(){
        $this->load->model('DBmodel');
        $tab=array();
        $tab[0] = $this->input->post("id");
        $tab[1] = $this->input->post("nom");
        $tab[2] = $this->input->post("prix");
        $tab[3] = $this->input->post("image");
        $this->DBmodel->update_produit($tab);
        $cond=null;
        $data['listProd']=$this->DBmodel->getProduit($cond);
        $this->load->view('crud',$data);
    }

    public function delete(){
        $this->load->model('DBmodel');
        $id = $this->input->post("idDel");
        $this->DBmodel->deleteProduit($id);
        $cond=null;
        $data['listProd']=$this->DBmodel->getProduit($cond);
        $data['vue']='crud.php';
        $this->load->view('template',$data);
    }

    public function rechercher(){
		$this->load->model('model');
        $mot = $this->input->post("phrase");
        echo $mot;
		$data['liProdRe']=$this->model->search($mot);
        $data['vue']='pageRecherche.php';
        $this->load->view('template',$data);

	}
	

}

?>