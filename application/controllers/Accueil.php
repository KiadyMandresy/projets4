<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accueil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$this->load->helper('assets');
		$this->load->model('model');
		$data['licaisse']=$this->model->get_caisse();
		$data['vue']='accueil.php';
		$this->load->view('template',$data);

	}	

	public function choix(){
		$caisse=$this->input->post("caisse");
		$this->session->set_userdata('idCaisse',$caisse);
		redirect(site_url("Achat/saisie"));
	}

	public function AjouterPanier(){
		$this->load->model('model');
		$prod=$this->input->post("pd");
		$tab=array();
		$tab[0]=$prod;
		$tab[1]=$this->session->userdata('idCaisse');
		$tab[2]=$this->input->post("prix");
		$tab[3]=$this->input->post("qte");
		$tab[4]=0;
		

		$this->model->insert_achat($tab);
		redirect(site_url("Achat/saisie"));
	}

	

	
	
}
