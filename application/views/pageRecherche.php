<div class="cart-page">
        <div class="container-fluid">
            <div class="row">
            
                <div class="col-lg-8">
                    <div class="cart-page-inner">
                       
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Image</th>
                                            <th>Produit</th>
                                            <th>Prix</th>
                                        </tr>
                                    </thead>
                                    <?php for($i=0;$i<count($liProdRe);$i++) {?>
                                        <form action="<?php echo site_url('Accueil/AjouterPanier') ?>" method="POST"> 
                                        <tbody class="align-middle">
                                            <tr>
                                                <td>
                                                    <div class="img">
                                                        <a href="#"><img src="<?php echo img_url($liProdRe[$i]['image']) ?>" alt="Image"></a>
                                                        <p><input type="hidden" name="pd" value="<?php echo $liProdRe[$i]['idProd'] ?>" ><?php echo $liProdRe[$i]['produit'] ?></p>
                                                    </div>
                                                </td>
                                                <td><input type="hidden" name="prix" value="<?php echo $liProdRe[$i]['prix'] ?>" ><?php echo $liProdRe[$i]['prix'] ?></td>
                                                
                                            </tr>
                                        </tbody>
                                        </form>
                                    <?php } ?>
                                </table>
                            </div>
                       
                    </div>
                </div>