<div class="col-lg-12">    
    <div class="register-form">
        <div class="row">
            <form action="<?php echo site_url('BigController/insertProd') ?>" method="post">
                <div class="col-md-12">
                    <label>Nom</label>
                    <input class="form-control" type="text" name="nom" placeholder="nom">
                </div>
                <div class="col-md-12">
                    <label>Categorie</label>
                    <select class="form-control" name="categ" id="categ">
                        <?php for($i=0;$i<count($listCate);$i++) { ?>
                            <option value="<?php echo $listCate[$i]['idCateg'] ?>"><?php echo $listCate[$i]['categ'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <label>Prix</label>
                    <input class="form-control" type="text" name="prix" placeholder="prix">
                </div>
                <div class="col-md-12">
                    <label>Image</label>
                    <input class="form-control" type="text" name="image" placeholder="image">
                </div>
                <div class="col-md-12">
                    <button class="btn">Valider</button>
                </div>
            </form>

            <form action="<?php echo site_url('BigController/getUrlCrud') ?>" method="post">
                <div class="col-md-12">
                    <button class="btn">retour</button>
                </div>
            </form>

        </div>
    </div>
</div>