
  <div class="col-lg-3">
  <h3>Caisse numero <?php echo $idCaisse ?></h3>

    <br>
    <form action="<?php echo site_url('Accueil/choix') ?>" method="POST">   
        <div class="form-group">
            <label for="caisse"><h3>Choissir une categorie</h3></label>
            <select class="form-control" name="caisse" id="caisse">
                <?php for($i=0;$i<count($liCateg);$i++) { ?>
                    <option value="<?php echo $liCateg[$i]['idCateg'] ?>"><?php echo $liCateg[$i]['categ'] ?></option>
                <?php } ?>
            </select>
        </div>
        <Button class="btn btn-primary mb-5">Valider</Button>
    </form>     

  </div>

  <div class="cart-page">
        <div class="container-fluid">
            <div class="row">
            
                <div class="col-lg-8">
                    <div class="cart-page-inner">
                       
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Add</th>
                                        </tr>
                                    </thead>
                                    <?php for($i=0;$i<count($liProd);$i++) {?>
                                        <form action="<?php echo site_url('Accueil/AjouterPanier') ?>" method="POST"> 
                                        <tbody class="align-middle">
                                            <tr>
                                                <td>
                                                    <div class="img">
                                                        <a href="#"><img src="<?php echo img_url($liProd[$i]['image']) ?>" alt="Image"></a>
                                                        <p><input type="hidden" name="pd" value="<?php echo $liProd[$i]['idProd'] ?>" ><?php echo $liProd[$i]['produit'] ?></p>
                                                    </div>
                                                </td>
                                                <td><input type="hidden" name="prix" value="<?php echo $liProd[$i]['prix'] ?>" ><?php echo $liProd[$i]['prix'] ?></td>
                                                <td>
                                                    <div class="qty">
                                                    <input style="width:100px;background-color:gray;" type="number" name="qte" step="1" min="0" max="100">
                                                    </div>
                                                </td>
                                                <td><button class="btn-cart">Add to Cart</button></td>
                                            </tr>
                                        </tbody>
                                        </form>
                                    <?php } ?>
                                </table>
                            </div>
                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="cart-page-inner">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <div class="cart-summary">
                                        
                                    <div class="cart-btn">
                                   

                                    <form action="<?php echo site_url('Achat/ListePanier') ?>" method="POST"> 
                                        <button>Panier</button>
                                    </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- /.col-lg-3 -->

  <div class="col-lg-9">

    <div class="row ">

    </div>
    <!-- /.row -->

  </div>
  <!-- /.col-lg-9 -->

</div>
<!-- /.row -->

</div>
<!-- /.container -->