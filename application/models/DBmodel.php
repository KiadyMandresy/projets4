<?php if(!defined('BASEPATH')) exit('No direct script acces allowed');
class DBmodel extends CI_Model{

    public function get_caisse(){
        $query=$this->db->query('select * from caisse');
        $list=array();
        foreach ($query->result_array() as $row){
            $list[]=$row;
            
        }
        return $list;
    }

    public function get_prod(){
        $query=$this->db->query('select * from produit');
        $list=array();
        foreach ($query->result_array() as $row){
            $list[]=$row;
        }
        return $list;
    }

    public function getOneProd($id){
        $sql="select * from produit where idProd='%s'";
        $sql = sprintf($sql,$id);
        $query= $this->db->query($sql);
        $list=array();
        foreach ($query->result_array() as $row){
            $list[]=$row;
        }
        return $list;
    }

    public function get_categ(){
        $query=$this->db->query('select * from categorie');
        $list=array();
        foreach ($query->result_array() as $row){
            $list[]=$row;
        }
        return $list;
    }

    public function getListAdmin(){
        $query= $this->db->query('SELECT * FROM admin');
        foreach($query->result_array() as $row){
            $tab[]=$row;
        }
        return $tab;
    }

    public function insertProduit($tab){
        $req = "INSERT INTO produit(produit,idCateg,prix,image) 
            VALUES ('%s',%s,%s,'%s')";
        $req = sprintf($req,$tab[0],$tab[1],$tab[2],$tab[3]);
        $query=$this->db->query($req);
    }    

    public function getProduit($cond=null){
        $sql = "SELECT * FROM produit join categorie on produit.idcateg=categorie.idcateg";
        if($cond!=null) {
            $sql = $sql+" where idCategorie='%s'";
            $sql = sprintf($sql,$cond);
        }
        $query= $this->db->query($sql);
        $tab=array();
            $i=0;
            foreach($query->result_array() as $row){
                $tab[]=$row;
            }
            return $tab;
    }
    public function update_produit($tab){
        $req= "UPDATE produit set produit='%s', prix='%s', image='%s' 
        where idProd='%s'";
        $req=sprintf($req,$tab[1],$tab[2],$tab[3],$tab[0]);
        $query=$this->db->query($req);
    }

    public function deleteProduit($id){
        $req = "DELETE from produit where idProd=%s";
        $req =sprintf($req,$id);
        $query=$this->db->query($req);
    }



}


?>