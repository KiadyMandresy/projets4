<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
    // require('Mere.php');
    class Model extends CI_Model{

        public function get_caisse(){
            $query=$this->db->query('select * from caisse');
            $list=array();
            foreach ($query->result_array() as $row){
                $list[]=$row;
            }
            return $list;
        }

        public function get_prod(){
            $query=$this->db->query('select * from produit');
            $list=array();
            foreach ($query->result_array() as $row){
                $list[]=$row;
            }
            return $list;
        }

        public function get_Categ(){
            $query=$this->db->query('select * from categorie');
            $list=array();
            foreach ($query->result_array() as $row){
                $list[]=$row;
            }
            return $list;
        }

        public function get_IdProdbyName($name){
            $query=$this->db->query('select * from produit where produit='.$name);
            $list=array();
            foreach ($query->result_array() as $row){
                return $list[0];
            }
            
        }

        public function insert_achat($tab){
            $req = "INSERT INTO achat(idProd,idCaisse,prix,qte,validite)
            VALUES (%s,%s,%s,'%s',%s)";
            $req = sprintf($req,$tab[0],$tab[1],$tab[2],$tab[3],$tab[4]);
            $query=$this->db->query($req);
            echo($req);
        }

        public function get_AchatbyIdCaisse($idCaisse){
            $query=$this->db->query('select * from Achat where idCaisse='.$idCaisse);
            $list=array();
            foreach ($query->result_array() as $row){
                $list[]=$row;
            }
            return $list;
        }
        public function search($mot){
            $query=$this->db->query("Select * from produit where lower(produit) like '%$mot% or like $mot% or like %$mot'");
            $list=array();
            foreach ($query->result_array() as $row){
                $list[]=$row;
            }
            return $list;
        }

        
public function validate_achat()
{
    $req="update achat set validite=1";
    $query=$this->db->query($req);
}

public function get_produit_achat_categorie($args){
    $req="select a.idAchat,a.idProd,p.produit,a.prix as prixTotal,a.qte,a.validite,a.idCaisse,p.image,p.prix as pUnitaire,c.categ
     from achat a join produit p on a.idProd=p.idProd 
     join categorie c on c.idCateg=p.idCateg where idCaisse =".$args['idCaisse']." and validite = ".$args['validite'];
    $query=$this->db->query($req);
    $tab=array();
        $i=0;
        foreach($query->result_array() as $row){
            $tab[$i]['idAchat']=$row['idAchat'];
            $tab[$i]['idProd']=$row['idProd'];
            $tab[$i]['produit']=$row['produit'];
            $tab[$i]['prix']=$row['prixTotal'];
            $tab[$i]['qte']=$row['qte'];
            $tab[$i]['validite']=$row['validite'];
            $tab[$i]['idCaisse']=$row['idCaisse'];
            $tab[$i]['image']=$row['image'];
            $tab[$i]['pUnitaire']=$row['pUnitaire'];
            $tab[$i]['categ']=$row['categ'];
            //$tab[]=$row
            $i++;
        }
        

        return $tab;  
}

    }


?>