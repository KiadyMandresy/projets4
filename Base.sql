create database minisupermarcheS4;
use minisupermarcheS4;

create table Admin(
    login varchar(50),
    mdp varchar(50)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

create table categorie(
    idCateg int not null AUTO_INCREMENT,
    categ varchar(50),
    PRIMARY KEY(idCateg)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

create table produit(
    idProd int not null AUTO_INCREMENT,
    produit varchar(50),
    idCateg int,
    prix float,
    image VARCHAR(50),
    PRIMARY KEY(idProd),
    FOREIGN KEY(idCateg) references Categorie(idCateg)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

create table caisse(
    idCaisse int not null AUTO_INCREMENT,
    nom varchar(50),
    PRIMARY KEY(idCaisse)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

create table achat(
    idAchat int not null AUTO_INCREMENT,
    idProd int,
    idCaisse int,
    prix float,
    qte int,
    validite int,
    PRIMARY KEY(idAchat),
    FOREIGN KEY(idProd) references produit(idProd),
    FOREIGN KEY(idCaisse) references caisse(idCaisse)
)ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

insert into Admin values('admin','admin');

insert into caisse(nom) values('Caisse1');
insert into caisse(nom) values('Caisse2');
insert into caisse(nom) values('Caisse3');

insert into categorie values(1,'legumes');
insert into categorie values(2,'snacks');
insert into categorie values(3,'boisson');

insert into produit(produit,idCateg,prix,image) values('Carotte',1,2300,'carotte.jpg');
insert into produit(produit,idCateg,prix,image) values('Tomate',1,2500,'tomate.jpg');
insert into produit(produit,idCateg,prix,image) values('Oignon',1,1700,'oignon.jpg');
insert into produit(produit,idCateg,prix,image) values('Chou-fleur',1,2200,'chou-fleur.jpg');
insert into produit(produit,idCateg,prix,image) values('Brocoli',1,3000,'brocoli.jpg');

insert into produit(produit,idCateg,prix,image) values('Pringles',2,12000,'pringles.jpg');
insert into produit(produit,idCateg,prix,image) values('Speculoos',2,8000,'speculoos.jpg');
insert into produit(produit,idCateg,prix,image) values('Oreo',2,9000,'oreo.jpg');
insert into produit(produit,idCateg,prix,image) values('Cookies',2,15000,'cookies.jpg');
insert into produit(produit,idCateg,prix,image) values('Kinder',2,10000,'kinder.jpg');

insert into produit(produit,idCateg,prix,image) values('Coca cola',3,5000,'coca.jpg');
insert into produit(produit,idCateg,prix,image) values('Fanta',3,5000,'fanta.jpg');
insert into produit(produit,idCateg,prix,image) values('Monster',3,6000,'monster.jpg');
insert into produit(produit,idCateg,prix,image) values('Eau Vive',3,2200,'eauvive.jpg');
insert into produit(produit,idCateg,prix,image) values('Ice Tea',3,7000,'icetea.jpg');
